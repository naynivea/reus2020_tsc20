/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import JUnit.Junit09_Geometria.dto.Geometria;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
class GeometriaTest {

	@Test
	void testObjeto() {
		Geometria g = new Geometria();
	}

	@Test
	void testObjetoParametro() {
		Geometria g = new Geometria(10);
	}
	
	@Test
	void testAreaCuadrado() {
		Geometria g = new Geometria();
		int resultado = g.areacuadrado(2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaCirculo() {
		Geometria g = new Geometria();
		double resultado = g.areaCirculo(2);
		double esperado = 12.56;
		assertEquals(esperado, resultado, 1);
	}
	
	@Test
	void testAreatriangulo() {
		int resultado = Geometria.areatriangulo(2,5);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRetangulo() {
		Geometria g = new Geometria();
		int resultado = g.arearectangulo(4,6);
		int esperado = 24;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaPentagono() {
		Geometria g = new Geometria();
		int resultado = g.areapentagono(6,7);
		int esperado = 21;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRombo() {
		Geometria g = new Geometria();
		int resultado = g.arearombo(5,4);
		int esperado = 10;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaRomboide() {
		Geometria g = new Geometria();
		int resultado = g.arearomboide(2,2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testAreaTrapecio() {
		Geometria g = new Geometria();
		int resultado = g.areatrapecio(2,4,6);
		int esperado = 18;
		assertEquals(esperado, resultado);
	}
	
	private static Stream<Arguments> getFormatFixture() {
		return Stream.of(
			Arguments.of(1),
			Arguments.of(2),
			Arguments.of(3),
			Arguments.of(4),
			Arguments.of(5),
			Arguments.of(6),
			Arguments.of(7),
			Arguments.of(8)
		);
	}
	
	@ParameterizedTest
	@MethodSource("getFormatFixture")
	void testFigura(int fiCode) {
		Geometria g = new Geometria();
		String resultado = g.figura(fiCode);
		String esperado = "Circulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testToString() {
		Geometria g = new Geometria(2);
		String resultado = g.toString();
		String esperado = "Geometria [id=2, nom=Circulo, area=0.0]";
		assertEquals(esperado, resultado);
	}
}
